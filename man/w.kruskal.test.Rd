% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/descsuppR-package.r
\name{w.kruskal.test}
\alias{w.kruskal.test}
\title{kruskal.test with unified interface}
\usage{
w.kruskal.test(values, grouping, ...)
}
\arguments{
\item{values}{vector.  The values to compare (age, toxicity score, gene expression, ...)}

\item{grouping}{vector of the same length as values.  treated as factor giving the
group membership}

\item{...}{additional parameters.  are passed on to the kruskal.test}
}
\value{
the value kruskal.test
}
\description{
kruskal.test with unified interface
}
\details{
just a call to kruskal.test that unifies the api to other tests
}
\author{
Andreas Leha
}
